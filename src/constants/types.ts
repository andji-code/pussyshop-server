export type MongoItem = {
  _id: string
  _v: number
}
