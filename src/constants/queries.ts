import { ProductType } from './../tools/db/products'
import { Product } from '../tools/db/products'
import { Cart, CartItemType } from '../tools/db/cart'

export const countTotalPrice = async (cart: CartItemType[]) => {
  const inCartProductIds = cart.map((cartItem) => cartItem.productId)
  const inCartProducts = await Product.find().where('_id').in(inCartProductIds).exec()
  const inCartProductMap = {} as { [key: ProductType['_id']]: ProductType }
  inCartProducts.forEach((p) => (inCartProductMap[p._id] = p))

  let totalPrice = 0
  cart.forEach(({ productId, count }) => (totalPrice += inCartProductMap[productId].price * count))

  return totalPrice
}

export const getCart = async () => {
  const cart = await Cart.find({}).exec()

  const items: { [key: ProductType['_id']]: number } = {}
  const totalPrice = await countTotalPrice(cart)

  cart.forEach((item) => {
    items[item.productId] = item.count
  })

  return { items, totalPrice }
}
