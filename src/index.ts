import express from 'express'
import http from 'http'
import env from 'dotenv'
import cors from 'cors'

import { mongoDbConnect } from './tools/db'
import { main } from './constants/main'
import bodyParser from 'body-parser'
import { homeRoute } from './api/home'
import { productRoute } from './api/product'
import { cartRoute } from './api/cart'
import { orderRoute } from './api/order'

env.config()
main.rootDir = __dirname

const PORT = process.env.PORT
const app = express()

app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const db = mongoDbConnect((db) => {
  console.log('db accessed success! ^w^')
  // прослушиваем прерывание работы программы (ctrl-c)
  process.on('SIGINT', () => {
    db.close()
    process.exit()
  })
})

app.use('/', homeRoute)
app.use('/products', productRoute)
app.use('/cart', cartRoute)
app.use('/orders', orderRoute)

app.use('/public', express.static(`${main.rootDir}/public`))

const server = http.createServer(app)

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}...`)
})
