import { main } from '../constants/main'
import express from 'express'
import { Order, OrderType } from '../tools/db/order'
const route = express.Router()

route.post('/', (req, res) => {
  const data: OrderType = req.body
  const order = new Order(data).save()
  res.json(order)
})

export const orderRoute = route
