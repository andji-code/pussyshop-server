import { main } from '../constants/main'
import express from 'express'
const route = express.Router()

route.get('/', (req, res) => {
  res.status(200).sendFile(`${main.rootDir}/public/meow.html`)
})

export const homeRoute = route
