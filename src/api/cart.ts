import { countTotalPrice, getCart } from './../constants/queries'
import { Product, ProductType } from './../tools/db/products'
import { Cart, CartItemType } from './../tools/db/cart'
import express from 'express'

const route = express.Router()

route.get('/', async (req, res) => {
  const { items, totalPrice } = await getCart()

  res.json({ items, totalPrice })
  console.log({ items, totalPrice })
})

route.post('/:id', async (req, res) => {
  const productId: ProductType['_id'] = req.params.id
  const cartItem = await Cart.findById(productId).exec()

  if (!cartItem) {
    const newCartItem: CartItemType = {
      count: 1,
      productId
    }
    const item = await new Cart(newCartItem).save()

    const { items, totalPrice } = await getCart()

    res.json({ items, totalPrice })
    return
  }

  res.status(404).send(`product: ${productId} already in the cart`)
})

route.put('/:id', async (req, res) => {
  const productId: ProductType['_id'] = req.params.id
  console.log({ req: req })

  console.log({ productId })

  const cartItem = await Cart.findOne({ productId }).exec()
  console.log({ cartItem })
  if (cartItem) {
    console.log('cartItem-PUT')
    cartItem.count += 1
    const item = await cartItem.save()

    const { items, totalPrice } = await getCart()

    res.json({ items, totalPrice })
    return
  }
  console.log('cartItem-PUT-error')
  res.status(404).json(null)
})

route.delete('/:id', async (req, res) => {
  const productId: ProductType['_id'] = req.params.id
  const cartItem = await Cart.findOne({ productId }).exec()
  console.log({ cartItem })
  if (cartItem) {
    if (cartItem.count === 1) {
      const item = await cartItem.remove()
      const { items, totalPrice } = await getCart()
      res.json({ items, totalPrice })
      return
    }
    cartItem.count -= 1
    const item = await cartItem.save()
    const { items, totalPrice } = await getCart()
    res.json({ items, totalPrice })
    return
  }
})

export const cartRoute = route
