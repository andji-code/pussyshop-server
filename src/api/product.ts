import { main } from './../constants/main'
import { Product } from './../tools/db/products'
import type { ProductType } from './../tools/db/products'
import express from 'express'
const route = express.Router()

route.get('/', async (req, res) => {
  const allProducts = await Product.find({}).exec()
  res.json(allProducts)
})

route.post('/add', async (req, res) => {
  const productData: ProductType = req.body
  console.log({ productData })
  console.log({ req: req })
  const product = await new Product(productData).save()
  console.log({ product })
  res.json(product)
})

export const productRoute = route
