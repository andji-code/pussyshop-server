import { MongoItem } from './../../constants/types'
import { ProductType } from './products'
import { model, Schema } from 'mongoose'

export type OrderItem = {
  count: number
  productId: ProductType['_id']
}

type User = {
  name: string
  surname: string
  address: string
  phone: string
}

export type OrderType = {
  order: OrderItem[]
} & User

const orderItemSchema = new Schema<OrderItem>({
  productId: {
    type: String,
    required: true
  },
  count: {
    type: Number,
    required: true
  }
})

const orderSchema = new Schema<OrderType>({
  address: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  order: {
    type: [orderItemSchema],
    required: true
  }
})

export const Order = model<OrderType>('Order', orderSchema)
