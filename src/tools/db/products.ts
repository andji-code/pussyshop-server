import { MongoItem } from './../../constants/types'
import { model, Schema } from 'mongoose'

export type ProductType = {
  name: string
  price: number
  img: string
} & MongoItem

const productSchema = new Schema<ProductType>({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  }
})

export const Product = model<ProductType>('Product', productSchema)
