import { MongoItem } from './../../constants/types'
import { ProductType } from './products'
import { model, Schema } from 'mongoose'

export type CartItemType = {
  productId: MongoItem['_id']
  count: number
}

const cartItemSchema = new Schema<CartItemType>({
  productId: {
    type: String,
    required: true
  },
  count: {
    type: Number,
    required: true
  }
})

export const Cart = model<CartItemType>('CartItem', cartItemSchema)
